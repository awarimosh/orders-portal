import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, retryWhen, delay, scan, retry } from 'rxjs/operators';

import { environment } from '../environments/environment';

@Injectable({ providedIn: 'root' })
export class ProductService {

  constructor(private http: HttpClient) { }

  readAll(): Observable<any> {
    !environment.production && console.log("ProductService.readAll");

    return this.http.get<any>(`${environment.base_api}/products`)
      .pipe(
        retry(1),
        map(data => {
          return data
        }),
        catchError((err: any) => {
          return observableOf(err);
        })
      )
  }

  readOne(_id: string): Observable<any> {
    !environment.production && console.log("ProductService.readOne");

    return this.http.get<any>(`${environment.base_api}/products/${_id}`)
      .pipe(
        retry(1),
        map(data => {
          return data
        }),
        catchError((err: any) => {
          return observableOf(err);
        })
      )
  }

  create(advertiserObj: any): Observable<any> {
    !environment.production && console.log("ProductService.create");

    return this.http.post<any>(`${environment.base_api}/products`, advertiserObj)
      .pipe(
        retry(1),
        map(data => {
          return data
        }),
        catchError((err: any) => {
          return observableOf(err);
        })
      )
  }

  update(_id: string, advertiserObj: any): Observable<any> {
    !environment.production && console.log("ProductService.update");

    return this.http.put<any>(`${environment.base_api}/products/${_id}`, advertiserObj)
      .pipe(
        retry(1),
        map(data => {
          return data
        }),
        catchError((err: any) => {
          return observableOf(err);
        })
      )
  }

  delete(_id: string): Observable<any> {
    !environment.production && console.log("ProductService.delete");

    return this.http.delete<any>(`${environment.base_api}/products/${_id}`)
      .pipe(
        retry(1),
        map(data => {
          return data
        }),
        catchError((err: any) => {
          return observableOf(err);
        })
      )
  }
}
