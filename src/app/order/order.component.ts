import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable} from '@angular/material/table';
import {OrderService} from "../order.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ProductService} from "../product.service";
import {MatSnackBar} from "@angular/material/snack-bar";

export interface OrderItem {
  products: OrderProduct[];
  amount: number;
  state: string;
  date: string;
}

export interface OrderProduct {
  id: number;
  name: string;
  description: string;
  price: number;
}

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements AfterViewInit, OnInit {
  dataSource: any;
  allProducts: any;
  loading = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<OrderItem>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['products' , 'amount', 'state', 'date', 'action'];

  constructor(private orderService: OrderService,
              private productService: ProductService,
              public dialog: MatDialog,
              private router: Router,) {
  }

  ngOnInit() {
    this.loadOrders();
  }

  ngAfterViewInit() {
    this.productService.readAll().subscribe(data => {
      this.allProducts = data.result;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddOrderDialog, {
      width: '400px',
      data: {products: this.allProducts}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.loading = true;
        setTimeout(() => {
          this.orderService.readAll().subscribe(data => {
            this.dataSource = data.result;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = data.result;
            this.loading = false;
          });
        }, 3000)
      }
    });
  }

  reload(): void {
    this.loading = true;
    setTimeout(() => {
      this.orderService.readAll().subscribe(data => {
        this.dataSource = data.result;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = data.result;
        this.loading = false;
      });
    }, 500)
  }

  loadOrders() {
    this.orderService.readAll().subscribe(data => {
      console.log('read all result', data);
      this.dataSource = data.result;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = data.result;
    });
  }

  cancelOrder(order): void {
    const dialogRef = this.dialog.open(CancelOrderDialog, {
      width: '400px',
      data: order
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        console.log('The dialog was closed', result);
        this.loading = true;
        setTimeout(() => {
          this.orderService.readAll().subscribe(data => {
            console.log('read all result', data);
            this.dataSource = data.result;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = data.result;
            this.loading = false;
          });
        }, 3000)
      }
    });
  }

  exit(): void{
    this.router.navigate(['/home']);
  }
}

@Component({
  selector: 'add-order-dialog',
  templateUrl: 'add-order-dialog.html',
})
export class AddOrderDialog implements OnInit{
  public selectedProduct: any;
  public cost: any;
  public quantity: any;

  constructor(public dialogRef: MatDialogRef<AddOrderDialog>,
              @Inject(MAT_DIALOG_DATA) public data,
              private orderService: OrderService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.cost = 0;
    this.quantity = 0;
  }

  calculate(){
    if(this.selectedProduct){
      this.cost = this.selectedProduct.price * this.quantity;
    }
    else{
      this.cost = 0;
    }
  }

  onCancel(): void {
    this._snackBar.open('cancelled', null, {
      duration: 1500,
    });
    this.dialogRef.close();
  }

  onConfirm(): void {
    const data = {
      "amount" : this.cost,
      "date" : new Date().toISOString,
      "products" : [
        this.selectedProduct
      ]
    };
    if(this.cost > 0){
      this.orderService.create(data)
        .subscribe((response) => {
          if (response) {
            this.dialogRef.close(response);
            this._snackBar.open('success', null, {
              duration: 1500,
            });
            console.log('response', response);
          }
        });
    }
    else{
      this._snackBar.open('cost must bbe greater than 0', null, {
        duration: 2000,
      });
    }
  }

}

@Component({
  selector: 'cancel-order-dialog',
  templateUrl: 'cancel-order-dialog.html',
})
export class CancelOrderDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<CancelOrderDialog>,
              @Inject(MAT_DIALOG_DATA) public data,
              private orderService: OrderService,
              private _snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
    console.log('n data', this.data);
  }

  confirmCancel(order): void {
    this.orderService.update(order.id,{state: 'cancelled'})
      .subscribe((response) => {
        if (response) {
          this.dialogRef.close(response);
          this._snackBar.open('success', null, {
            duration: 1500,
          });
          console.log('response', response);
        }
      });
  }

  dismiss(): void {
    this.dialogRef.close(null);
  }
}
