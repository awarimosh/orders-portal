import {Component, Inject, OnInit} from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import {ProductService} from "../product.service";
import {OrderService} from "../order.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

export interface Product {
  id: string;
  name: string;
  description: string;
  price: number;
}
export interface Order {
  products: Product[];
  amount: number;
  state: string;
  date: string;
}
@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit {

  /** Based on the screen size, switch from standard to one column per row */
  orders: any;
  products: any;
  loading = false;
  displayedColumnsProducts = ['position', 'name' , 'description', 'price'];
  displayedColumnsOrders = ['position', 'date', 'product' , 'amount', 'state'];
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Product List', cols: 1, rows: 1, id : 1 },
          { title: 'Order List', cols: 1, rows: 1, id : 2 },
        ];
      }

      return [
        { title: 'Product List', cols: 1, rows: 2, id : 1 },
        { title: 'Order List', cols: 1, rows: 2 , id : 2 },
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver,
              private orderService: OrderService,
              private productService: ProductService,
              public dialog: MatDialog,
              private router: Router,
  ) {

  }
  ngOnInit(): void {
    this.orderService.readAll().subscribe(data => {
      console.log('read all order result', data);
      this.orders = data.result;
    });

    this.productService.readAll().subscribe(data => {
      console.log('read all product result', data);
      this.products = data.result;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddProductDialog, {
      width: '400px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        console.log('The dialog was closed', result);
        this.loading = true;
        setTimeout(() => {
          this.productService.readAll().subscribe(data => {
            console.log('read all result', data);
            this.products = data.result;
            this.loading = false;
          });
        }, 3000)
      }
    });
  }

  addNew(): void {
      this.openDialog();
  }

  showOrders(): void {
    this.router.navigate(['/order']);
  }
}

@Component({
  selector: 'add-product-dialog',
  templateUrl: 'add-product-dialog.html',
})
export class AddProductDialog implements OnInit{
  public name: any;
  public description: any;
  public price: any;

  constructor(public dialogRef: MatDialogRef<AddProductDialog>,
              @Inject(MAT_DIALOG_DATA) public data,
              private productService: ProductService,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  onCancel(): void {
    this._snackBar.open('cancelled', null, {
      duration: 1500,
    });
    this.dialogRef.close();
  }

  onConfirm(): void {
    const data = {
      "name" : this.name,
      "description" : this.description,
      "price" : this.price
    };
    if(this.price > 0){
      this.productService.create(data)
        .subscribe((response) => {
          if (response) {
            this.dialogRef.close(response);
            this._snackBar.open('success', null, {
              duration: 1500,
            });
            console.log('response', response);
          }
        });
    }
    else{
      this._snackBar.open('price must bbe greater than 0', null, {
        duration: 2000,
      });
    }
  }
}
